The outline of your christian testimomony

Your christian testimony is made up of four main parts. The first part is how your life was before christ became an important part of it. In the second part you should describe the event or circumstance that made you turn to Jesus as your savior. In the third part of your testimony you can tell how your life has been impacted, for the better, by becoming a christian. If you think you have some idea of what god's purpose for you, describe that in the fourth part of your testimony.


Before Christ entered my life

In this part of your christian testimony you explain what your life was like before you accepted Jesus as the major role of your life. Some people are born into their circumstances, some people slowly slip down into it and don't realize what is happening until it is to late. How was your life like? Were you lonley and scared? Did you feel you were neveer at peace? was it hare to be motivated? Were you depressed? Did you feel you had to control situations you were in? Did you feel there was no meaning in life, that something was missing, and you had no true purpose? These are all good questions to ask yourself when developing your testimony. Instead of giving your problems to god how did you deal with all these problems in your life? Did you drink or do durgs? Did you ignore your family? Were you a slave to money? Did you hang out with the wrong type of people? Did you throw yourself into just your work? Were you an exercise fanatic? Did you have a hobby or play video games that ruled your day? Were you addicted to sex? Was there something else that you used as a crutch to get through the day. In this second sub-part of your tesimony of how your life used to be explain how you satisied your missing deep inner needs.

https://www.testimonytree.com/t/the-power-of-testimonies/

Why I turned to Jesus as my savior

In this part of your christian testimony you should describe why you turned to Jesus as your savior. What event or events led you towards the path of christ? Some people turn to god a little at a time over events that seem to build up. Some people come to an understanding that god is there for them in a split second when everything is clear as day. Every one is called to jesus in a different way. In this part of your testimony you can tell how you were saved by the grace of god. You can use a quote from a particular passage here, or anywhere for that matter in your testimony. Also, this part of your testimony is wher you would tell about what you did and currenttly are doing in your walk towards christ. At the end of this part of my christian testimony I added my core christian beliefs: I Aknowledge all have sinned, I beleive Jesus died for our sins so that they could be forgiven, Christ is our savior, and I am saved to serve.

testimony in the bible
christian testimony example
written testimony example
how to write a christian testimony
christian testimony books
christian testimonies 2018
christian testimony in english
christian testimony tamil

How has my life been impacted by becoming a christian

In this part of your christian testimony you can explain how your life has been impacted by turning to Jesus and becoming a christian. Are you now at peace with things? Did you ask for forgiveness and did you forgive? How have your old habits changed? Have you put old relationships away that were not good for you and built new ones with people that believe in christ? Do you feel less anxiety over a situation and realize you are not the one in control. Do you give your problems to god to handle and ask him for guidance? Are you involved in the church community? Are you attending or leading a bible study? Do you help a charity? Are you testifying to help bring other people to christ? How is your pesonel relationship with god now that you have the promise of eternal life?

testimony in the bible
testimonies of faith
written testimony example
christian testimony example
personal faith statement examples
how to write your testimony worksheet
testimonies today
salvation testimonies
testimony share
testimonies of god doing the impossible
testimonies of answered prayer
jesus saved my life
personal testimony of salvation
christian testimony books
testimony service
what is a testimony in church
church testimony example
famous christian testimonies
christian testimony of a transformed life
dramatic conversions to christianity
powerful testimonies of deliverance
call to ministry testimony
christian testimony tamil
christian testimony in hindi
famous christian conversion stories
christian testimony in english
how to write a compelling testimony
thank you lord testimony
sermon on personal testimony
creative ways to share your testimony
testimony in the scriptures
different types of testimonies
personal faith journey examples
i don't have a testimony
relevant magazine testimony
sermons about testimony
testimonies of being saved
